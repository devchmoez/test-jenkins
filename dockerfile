ARG DOCKER_IMAGE
#MAINTAINER Jonathan Gallegos <jonathan.gallegos-alegria@atos.net>
#FROM ${DOCKER_IMAGE} AS minimal
FROM catalog.docker.net-courrier.extra.laposte.fr/ubi7/base-php74-apache-kafka-mysql:latest AS minimal
ARG PHP_VER=php74

USER root
#COPY apache2-foreground docker-php-entrypoint wait-for-it /usr/local/bin/
#COPY ./etc/conf.d/ /etc/httpd/conf.d/
RUN  sed -i 's/^variables_order/; variables_order/g' /etc/opt/remi/${PHP_VER}/php.ini

FROM minimal AS builder
ARG WORKDIR
WORKDIR $WORKDIR

COPY . $WORKDIR
RUN touch .env \
    && composer install -o --no-dev --no-progress --no-scripts \
    && chown -R appuser:appuser . \
    && chmod -R a-rwx,uo+rX . \
    && chmod -R uo+x bin/console \
    && chmod -R uo+w var/*

#Image déployable dans un cluster
#FROM builder AS deploy
#ARG WORKDIR
#ARG PHP_VER
#USER root
#COPY --from=builder /usr/local/bin /usr/local/bin
#COPY --from=builder $WORKDIR/ $WORKDIR/
#COPY --from=builder /etc/httpd/conf.d/ /etc/httpd/conf.d/
#COPY --from=builder /etc/opt/remi/${PHP_VER}/php.ini /etc/opt/remi/${PHP_VER}
#USER appuser
#EXPOSE 8080
#ENTRYPOINT ["docker-php-entrypoint"]
#CMD ["apache2-foreground"]

#Pour lancer le projet dans une machine de dév, voir *docker-composer.yml*
FROM minimal AS dev
ARG WORKDIR=${APP_DATA}
RUN setfacl -R -dm o:rwX $WORKDIR
RUN yum update -y && yum upgrade -y && yum install -y iputils vi nmap-ncat # php74-php-pecl-xdebug3.x86_64 openssh-clients.x86_64 sshpass

RUN yum update -y && yum upgrade -y && yum install -y npm node
#RUN npm install

# XDebug
#ARG PHP_IDE_CONFIG="serverName=bootstrap-rest-api.localhost"
#ARG XDEBUG_REMOTE_HOST='host.docker.internal'
#RUN yum --disableplugin=subscription-manager --setopt=obsoletes=0 install -y ${PHP_VER}-php-pecl-xdebug
#RUN echo "xdebug.mode=debug,develop,trace" >> ${PHP_SYSCONF_PATH}/php.d/99-xdebug.ini \
#&& echo 'xdebug.remote_enable=1' >> ${PHP_SYSCONF_PATH}/php.d/99-xdebug.ini \
#&& echo 'xdebug.remote_connect_back=0' >> ${PHP_SYSCONF_PATH}/php.d/99-xdebug.ini \
#&& echo "xdebug.remote_host=${XDEBUG_REMOTE_HOST}" >> ${PHP_SYSCONF_PATH}/php.d/99-xdebug.ini \
#&& echo 'xdebug.discover_client_host=1' >> ${PHP_SYSCONF_PATH}/php.d/99-xdebug.ini \
#&& echo 'xdebug.remote_autostart=1' >> ${PHP_SYSCONF_PATH}/php.d/99-xdebug.ini \
#&& echo 'xdebug.remote_log=/tmp/xdebug.log' >> ${PHP_SYSCONF_PATH}/php.d/99-xdebug.ini

EXPOSE 8080 9000
CMD bash -c "[ ! -d \"vendor\" ] && php -d memory_limit=-1 /usr/bin/composer install -vvv ; httpd -D FOREGROUND"
